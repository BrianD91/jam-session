**Jam Session**

I created this application to prove myself that I could build something with the MERN stack. It's a simple social media app for musicians!

**Installation**

To install, simply clone the repository, cd into it and run the following commands

`npm install && npm run client-install`

That should get all the packages installed properly at which point you can run

`npm run dev`

To get the setup running locally. The API/server will run on port 5000 and the front-end runs on port 3000,
so once everything is running open your browser to http://localhost:3000 and you should see the application.